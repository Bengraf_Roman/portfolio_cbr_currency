<?php

class Config
{
    protected static $config = [
        'mysql' => [
            'host' => '',
            'port' => '',
            'dbname' => '',
            'user' => '',
            'password' => ''
        ],
    ];

    public static function getMysqlDsn()
    {
        $config = self::$config['mysql'];
        return 'mysql:host=' . $config['host']
            . ';port='. $config['port']
            . ';dbname='. $config['dbname']
            . ';charset=UTF8';
    }

    public static function getMysqlUser()
    {
        $config = self::$config['mysql'];
        return $config['user'];
    }

    public static function getMysqlPass()
    {
        $config = self::$config['mysql'];
        return $config['password'];
    }
}