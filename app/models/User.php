<?php
namespace App\Mvc\Model;
/**
 * Created by PhpStorm.
 * User: Bengraf
 * Date: 05.11.2016
 * Time: 18:22
 */
class User extends BaseModel
{
    protected static $user_data_path = 'auth/user_data.json';
    protected static $user_data;

    public $id;
    public $login;
    public $name;


    public static function getData()
    {
        $auth_data_json = file_get_contents(self::$user_data_path);
        static::$user_data = json_decode($auth_data_json, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    public static function checkUser($login = null, $password = null)
    {
        self::getData();
        if ($auth_data = self::$user_data) {
            return in_array([
                'login' => $login,
                'password' => $password
            ], $auth_data);
        } else {
            return (Object)[];
        }
    }

    public static function findUser($conditions)
    {
        self::getData();
        $user = (object)static::$user_data[array_search($conditions, static::$user_data)];
        unset($user->password);
        return $user;
    }
}