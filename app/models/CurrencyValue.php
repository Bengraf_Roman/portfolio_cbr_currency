<?php
namespace App\Mvc\Model;


/**
 * Класс курсы валют
 * Class CurrencyValue
 * @package App\Mvc\Model
 */
class CurrencyValue extends BaseModel
{
    protected static $table = 'currency';

    /* Код валюты */
    public $v_code;

    public function __construct()
    {
        parent::__construct();
    }

    public function __call($name, $arguments)
    {
    }

    /* Курс */
    public $v_value;

    /* timestamp обновления */
    public $date_update;

    /* DateTime обновления  d.m.Y */
    public $date_day_update;

    /**
     * Добавление / Обновление записи по валюте
     * @param $v_code
     * @param $v_value
     * @param $date_day_update
     */
    public static function createItem($v_code, $v_value, $date_day_update)
    {
        if (!$c = CurrencyValue::findOne([
            'v_code' => $v_code,
            'date_day_update' => (string)date('d.m.Y', strtotime($date_day_update))
        ])
        ) {
            $c = new static();
        }

        $c->v_code = $v_code;
        $c->v_value = $v_value;
        $c->date_update = time();
        $c->date_day_update = date('d.m.Y', strtotime($date_day_update ?? time()));
        $c->save();
    }
}