<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type="text/css" href="/public/css/promo.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="/public/js/promo.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>
<body>
<div class="bg-grey shadow">
    <div class="w-container">
        <div class="w-row">
            <div class="w-col w-col-6">
                <div class="logo"></div>
            </div>
            <div class="w-col w-col-6">
                <div class="v-flex w-row">
                    <div class="w-col w-col-5"><a class="label" href="#">8 800 200 200</a>
                        <div class="text-xs">горячая линия</div>
                    </div>
                    <div class="w-col w-col-5"><a class="label" href="#">Войти</a>
                        <div class="text-xs">в интернет-банк</div>
                    </div>
                    <div class="m-hidden w-col w-col-2">
                        <div class="image_logo"></div>
                    </div>
                </div>
            </div>
        </div>
        <h1 class="h-lg">В команде Точки <br>настоящие чудеса <br>происходят между людьми</h1><img
            src="/public/images/people.png">
    </div>
</div>
<div>
    <div class="w-container">
        <h2 class="h-lg padding-bottom-md">Начни работать <br>в надёжной компании <br>с духом стартапа</h2><img
            class="bottom-right m-hidden" src="/public/images/cube.jpg" width="50%">
        <p class="padding-bottom-md text-md">Мы верим, что только счастливые сотрудники могут сделать счастливыми
            клиентов.</p>
        <p class="padding-bottom text-xs">Горизонтальная структура, отсутствие строгих регламентов,
            <br>дресс-кода, инструкций и бюрократии. Стабильная и белая зарплата.
            <br>
            <br>Мы можем дать тебе многое и вместе перевернуть мир!</p>
    </div>
</div>
<div class="bg-grey">
    <div class="w-container">
        <div>
            <h2 class="h-lg padding-bottom-md">Вакансии</h2>
            <div class="list pointer"><a class="dd-link" onclick="core.showList()">Екатеринбург</a>
                <div class="list-wrapper hidden" data-target="list">
                    <a class="dd-link inner" href="#">Москва</a>
                    <a class="dd-link inner" href="#">Санкт-Петербург</a>
                    <a class="dd-link inner" href="#">Волгоград</a>
                    <a class="dd-link inner" href="#">Воронеж</a>
                    <a class="dd-link inner" href="#">Казань</a>
                    <a class="dd-link inner" href="#">Краснодар</a>
                    <a class="dd-link inner" href="#">Красноярск</a>
                    <a class="dd-link inner" href="#">Нижний Новгород</a>
                    <a class="dd-link inner" href="#">Новосибирск</a>
                </div>
            </div>
        </div>
        <div class="padding-bottom padding-md">
            <div class="w-row">
                <div class="w-col w-col-6">
                    <div class="list-item">
                        <h3 class="h-md">Менеджер по работе с клиентами</h3>
                        <p class="text-xs">Мы ищем ребят, готовых сделать настоящее шоу из скучного открытия счетов.Мы
                            ищем ребят, готовых сделать настоящее шоу из скучного открытия счетов.</p>
                    </div>
                </div>
                <div class="w-col w-col-6">
                    <div class="list-item">
                        <h3 class="h-md">Менеджер по работе с партнёрами</h3>
                        <p class="text-xs">Нам нужны ребята, способные вкусно продавать наши продукты и искать
                            партнёров.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding-bottom padding-md">
            <div class="w-row">
                <div class="w-col w-col-6">
                    <div class="list-item">
                        <h3 class="h-md">Программист Java</h3>
                        <p class="text-xs">Нам нужен человек, который будет заниматься разработкой лучшего
                            интернет-банка на платформе Java EE</p>
                    </div>
                </div>
                <div class="w-col w-col-6">
                    <div class="list-item">
                        <h3 class="h-md">Специалист дистанционного обслуживания</h3>
                        <p class="text-xs">Мы ищем человека,единственная задача которого делать наших клиентов
                            счастливыми</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding-bottom padding-md">
            <div class="w-row">
                <div class="w-col w-col-6">
                    <div class="list-item">
                        <h3 class="h-md">Программист Java</h3>
                        <p>Нам нужен человек, который будет заниматься разработкой лучшего интернет-банка на платформе
                            Java EE</p>
                    </div>
                </div>
                <div class="w-col w-col-6">
                    <div class="list-item">
                        <h3 class="h-md">Специалист дистанционного обслуживания</h3>
                        <p class="text-xs">Мы ищем человека,единственная задача которого делать наших клиентов
                            счастливыми</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="padding-bottom padding-md"><a class="btn w-button" href="#">Все вакансии</a>
        </div>
    </div>
</div>
<div>
    <div class="w-container">
        <div class="slider">
            <div class="slider-arrow" onclick="core.slider(1)">
                <img src="/public/images/arrow.png">
            </div>
            <div class="slider-body" data-slide="current">
                <div class="padding-bottom v-flex w-row">
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                    <div class="w-col w-col-2"><img class="img-center" src="/public/images/dots.png">
                    </div>
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                </div>
                <p class="text-lg text-quote">Тебя проведут, познакомят со всей командой, расскажут историю Точки и
                    правила взаимодействия. Окружат заботой и теплом. У нас по-другому быть не может</p>
                <p class="label text-center">Константин Константинопольский</p>
                <div class="padding-bottom text-center text-xs">разработчик</div>
                <div class="v-flex w-row">
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                    <div class="w-col w-col-2"><img src="/public/images/guy.png">
                    </div>
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                </div>
            </div>
            <div class="slider-body hidden" data-slide>
                <div class="padding-bottom v-flex w-row">
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                    <div class="w-col w-col-2"><img class="img-center" src="/public/images/dots.png">
                    </div>
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                </div>
                <p class="text-lg text-quote">Текст тест</p>
                <div class="padding-bottom text-center text-xs">разработчик</div>
                <div class="v-flex w-row">
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                    <div class="w-col w-col-2"><img src="/public/images/guy.png">
                    </div>
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                </div>
            </div>
            <div class="slider-body hidden" data-slide>
                <div class="padding-bottom v-flex w-row">
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                    <div class="w-col w-col-2"><img class="img-center" src="/public/images/dots.png">
                    </div>
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                </div>
                <p class="text-lg text-quote">Какой-то текст</p>
                <div class="padding-bottom text-center text-xs">разработчик</div>
                <div class="v-flex w-row">
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                    <div class="w-col w-col-2"><img src="/public/images/guy.png">
                    </div>
                    <div class="w-col w-col-5">
                        <div class="separator"></div>
                    </div>
                </div>
            </div>
            <div class="slider-arrow" onclick="core.slider(0)"><img src="/public/images/arrow-right.png">
            </div>
        </div>
    </div>
</div>
<div class="bg-grey">
    <div class="w-container">
        <div class="form w-clearfix">
            <div class="form-body">
                <h2 class="h-form h-lg">Пора работать</h2>
                <div class="form-wrapper w-form">
                    <form data-name="Email Form" id="email-form" name="email-form">
                        <input class="form_control w-input" id="Phone" maxlength="256" name="Phone"
                               placeholder="+7 (000) 000 00 00" required="required" type="text">
                        <input class="form_control w-input" id="email" maxlength="256" name="email"
                               placeholder="Ваше имя" required="required" type="email">
                        <div class="form_file_control padding-bottom">Название файла.docx</div>
                        <a class="btn btn-block padding-bottom w-button" href="#">Отправить</a>
                    </form>
                    <div class="w-form-done">
                        <div></div>
                    </div>
                    <div class="w-form-fail">
                        <div></div>
                    </div>
                </div>
            </div>
            <div class="form left-chunk"></div>
            <div class="form right-chunk"></div>
        </div>
    </div>
</div>
<div class="bg-grey">
    <div class="w-container"></div>
</div>
<div class="bg-grey">
    <div class="w-container">
        <div>
            <div class="w-row">
                <div class="w-col w-col-4 w-col-small-4 w-col-tiny-tiny-stack">
                    <h3 class="label">Информация</h3>
                    <ul class="w-list-unstyled">
                        <li>
                            <p class="footer_nav">Тарифы</p>
                            <p class="footer_nav">Банкоматы</p>
                            <p class="footer_nav">Правила банковского обслуживания</p>
                            <p class="footer_nav">Документы для открытия счёта</p>
                            <p class="footer_nav">Положение о филиале</p>
                            <p class="footer_nav">Работа в Точке</p>
                            <p class="footer_nav">Акции</p>
                            <p class="footer_nav">Полезное</p>
                        </li>
                    </ul>
                </div>
                <div class="w-col w-col-4 w-col-small-4 w-col-tiny-tiny-stack">
                    <h3 class="label">Самое свежее</h3>
                    <ul class="w-list-unstyled">
                        <li>
                            <p class="footer_nav">Быстрые расчёты и удобны интернет банк</p>
                            <p class="footer_nav">Зарплата сотрудникам</p>
                            <p class="footer_nav">Валютные сделки</p>
                            <p class="footer_nav">Оплата по картам</p>
                        </li>
                    </ul>
                </div>
                <div class="w-col w-col-4 w-col-small-4 w-col-tiny-tiny-stack">
                    <h3 class="label">Контакты</h3>
                    <ul class="w-list-unstyled">
                        <li class="w-clearfix">
                            <p class="label">880020004</p>
                            <p class="footer_nav">Бесплатный звонок по России</p>
                            <p class="dd-link label">Обратная связь</p>
                        </li>
                    </ul>
                    <div class="instagramm social-media"></div>
                    <div class="facebook social-media"></div>
                    <div class="social-media vk"></div>
                    <div class="social-media tvitter"></div>
                </div>
            </div>
            <p class="footer_nav">ПАО Хантамансийский банк "Открытие" https://www.open.ru</p>
        </div>
    </div>
</div>
</body>
</html>