<!DOCTYPE html>
<html>
<head>
    <title><?php if (isset($caption)) {
            echo $caption;
        } else {
            echo 'Банк - Главная страница';
        } ?></title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/public/css/main.css">
    <link rel="stylesheet" type="text/css" href="/public/custom/fa/css/font-awesome.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="/public/js/common.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body>
<section class="content">
    <section class="container">
        <div class="box box-feedback">
            <div class="box-header text-right">
                <a class="btn btn-sm btn-default" href="/">КУРСЫ ВАЛЮТ</a>
                <a class="btn btn-sm btn-default" href="/index/analysys">АНАЛИТИКА</a>
                <button type="button" class="btn btn-sm btn-default btn-primary"
                        onclick="admin.transport('user','logout',{},admin.logout)">
                    ВЫЙТИ
                </button>
            </div>
            <?php include_once($content); ?>

            <div class="box-footer bg-black">

            </div>
        </div>
    </section>
</section>

</body>