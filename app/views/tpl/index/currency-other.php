<?php foreach ($currencies['day_before'] as $code => $cur): ?>
    <?php $data_b = $currencies['day_before'][$code]; ?>
    <?php $data_a = $currencies['last_day'][$code]; ?>
    <div class="row">
        <div class="col-sm-3 col-xs-12">
            <b title="<?= $data_a->Vname ?>"><?= $data_a->VchCode ?></b>
        </div>
        <div class="col-sm-4 col-xs-6">
            <?= $data_b->Vcurs ?>
        </div>
        <div class="col-sm-5 col-xs-6">
                &nbsp;
                <span class="text-md">
                    <?= floatval($data_a->Vcurs) ?>
                </span>
            <?php if (floatval($data_b->Vcurs) > floatval($data_a->Vcurs)) {; ?>
                <span class="badge bg-red pull-right">
                    &#x2193;
                </span>
            <?php } elseif (floatval($data_b->Vcurs) == floatval($data_a->Vcurs)) {; ?>
                <span class="badge bg-yellow pull-right">
                    &#x2192;
                </span>
            <?php } else {; ?>
                <span class="badge bg-green pull-right">
                    &#x2191;
                </span>
            <?php }; ?>
        </div>
    </div>
<?php endforeach; ?>