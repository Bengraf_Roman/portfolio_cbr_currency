<div class="box box-feedback brd-grey">
    <div class="box-header">
        <?=$title ?? 'No title';?>
    </div>
    <div class="box-body text-center">
        <div class="row">
            <div class="col-sm-3 col-xs-12">

            </div>
            <div class="col-sm-4 col-xs-6">
                <a class="text-xs"><?=$date['day_before'];?></a>
            </div>
            <div class="col-sm-5 col-xs-6">
                <a class="text-xs"><?=$date['last_day'];?></a>
            </div>
        </div>
        <?php foreach ($first_shown as $code):?>
            <?php $data_b = $currencies['day_before'][$code];?>
            <?php $data_a = $currencies['last_day'][$code];?>
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <b title="<?= $data_a->Vname ?>"><?= $data_a->VchCode ?></b>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <?= $data_b->Vcurs ?>
                </div>
                <div class="col-sm-5 col-xs-6">
                    &nbsp;
                    <span class="text-md">
                        <?= floatval($data_a->Vcurs) ?>
                    </span>
                    <?php if (floatval($data_b->Vcurs) > floatval($data_a->Vcurs)){;?>
                        <span class="badge bg-red pull-right">
                            &#x2193;
                        </span>
                    <?php } elseif (floatval($data_b->Vcurs) == floatval($data_a->Vcurs)){;?>
                        <span class="badge bg-yellow pull-right">
                            &#x2192;
                        </span>
                    <?php } else {;?>
                        <span class="badge bg-green pull-right">
                            &#x2191;
                        </span>
                    <?php };?>
                </div>
            </div>
        <?php endforeach;?>
        <div data-target="cur">
        </div>
    </div>
    <div class="box-footer text-center">
        <span class="text-xs pointer btn btn-default" data-btn="load-other" onclick="admin.transport('index','otherCurrencies',{}, admin.loadOtherCur)">ЕЩЕ</span>
        <span class="text-xs pointer btn btn-default" style="display: none" data-btn="hide-other" onclick="admin.hideOtherCur()">СКРЫТЬ</span>
    </div>
</div>