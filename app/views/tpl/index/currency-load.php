<form id="cur_form">
    <div class="box box-feedback brd-grey">
        <div class="box-header">
            <?= $title ?? 'No title'; ?>
            <button type="button" class="btn btn-xs btn-primary pull-right"
                    data-spinner='load_spinner'
                    onclick="admin.loadCurr();">
                ЗАГРУЗИТЬ
            </button>
        </div>
        <div class="box-body">
            <div class="row">
                <?php $col_count = 4; ?>
                <?php foreach ($currencies as $currency): ?>
                    <div class="col-xs-<?= 12 / $col_count ?>">
                        <button type="button" class="btn btn-xs btn-block btn-default"
                                data-id="<?= trim($currency->Vcode) ?>"
                                onclick="admin.checkCurr('<?= trim($currency->Vcode) ?>')">
                            <?= $currency->VcharCode ?? $currency->VEngname ?>
                        </button>
                        <input type="hidden" data-id="<?= trim($currency->Vcode) ?>" name="VcommonCode[]" value=""/>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="progress">
            <div data-pb="load_pb" class="progress-bar" role="progressbar" aria-valuenow="0"
                 aria-valuemin="0" aria-valuemax="100" style="width:0%">
            </div>
        </div>
    </div>
    </div>
</form>