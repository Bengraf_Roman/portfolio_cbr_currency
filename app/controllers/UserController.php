<?php
namespace App\Mvc\Controller;

use App\Mvc\Model\User;
use App\Session;

class UserController extends BaseController
{
    public function indexAction()
    {
        $this->load->view('auth', [],'clean_layout');
    }

    public function authAjax()
    {
        $needle_user = User::findUser(
            [
                'login' => $this->request->getPost('login'),
                'password' => md5($this->request->getPost('password'))
            ]
        );

        if ($needle_user) {
            Session::set('token', md5(md5($this->request->getPost('login'))));
            Session::set('key', md5($this->request->getPost('login')));
            Session::set('uid', (int)$needle_user->id);
            $this->setResult(['result' => true]);
        } else {
            $this->setResult(['result' => false]);
        }
    }

    public function logoutAjax()
    {
        if (Session::destroy()) {
            $this->setResult(['result' => true]);
        }
    }

}