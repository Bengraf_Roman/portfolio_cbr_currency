<?php
namespace App\Mvc\Controller;

use App\Libraries\Cbr;
use App\Mvc\Model\CurrencyValue;


class IndexController extends BaseController
{
    public function indexAction()
    {
        $last_day = Cbr::getLatestDateTime();
        $day_before = $last_day - 86400;
        $first_shown = [
            'USD', 'EUR'
        ];

        $this->load->view('index', [
            'caption' => 'Загрузка курсов валют',
            'data' => $this->load->tplGet('index/currency-main', [
                'title' => 'Курс валют на сегодня',
                'currencies' => [
                    'last_day' => Cbr::getLatestCurs(),
                    'day_before' => Cbr::getCursOnDate($day_before),
                ],
                'date' => [
                    'last_day' => date('d.m.Y', $last_day),
                    'day_before' => date('d.m.Y', $day_before),
                ],
                'first_shown' => $first_shown
            ])
        ]);
    }

    public function otherCurrenciesAjax()
    {
        $last_day = Cbr::getLatestDateTime();
        $day_before = $last_day - 86400;

        $this->setResult([
            'target' => 'cur',
            'result' => $this->load->tplGet('index/currency-other',
                [
                    'currencies' => [
                        'last_day' => Cbr::getLatestCurs(),
                        'day_before' => Cbr::getCursOnDate($day_before),
                    ],
                    'date' => [
                        'last_day' => date('d.m.Y', $last_day),
                        'day_before' => date('d.m.Y', $day_before),
                    ]
                ]
            )
        ]);
    }

    public function AnalysysAction()
    {
        $this->load->view('load', [
            'caption' => 'Загрузка курсов валют',
            'data' => $this->load->tplGet('index/currency-load', [
                'currencies' => Cbr::getCurrencies(),
                'title' => 'Загрузка курсов валют в базу',
                'period' => 10
            ])
        ]);
    }

    public function loadCurrencyAjax()
    {
        $vcommonCode = $this->request->getPost('VcommonCode');
        $y_index = (int)$this->request->getPost('y');

        $res = Cbr::get('GetCursDynamicXML', [
            'FromDate' => Cbr::datetime(time() + 86400 * 365 * ($y_index - 4)),
            'ToDate' => Cbr::datetime(time() + 86400 * 365 * ($y_index - 3)),
            'ValutaCode' => $vcommonCode
        ]);

        if (!isset($res->ValuteCursDynamic)) {
            $this->setResult(['message' => 'Load ' . $vcommonCode . ' failed']);
            return;
        }

        foreach ($res->ValuteCursDynamic as $item) {
            CurrencyValue::createItem((string)$item->Vcode, (float)$item->Vcurs, (string)$item->CursDate);
        }

        //$this->setResult($res);

    }

    public function loadCurrenciesAjax()
    {
        $vcommonCodes = $this->request->getPost('VcommonCode');

        foreach ($vcommonCodes as $vcommonCode) {

            if (!$vcommonCode) continue;

            $res = Cbr::get('GetCursDynamicXML', [
                'FromDate' => Cbr::datetime(time() - 86400 * 365 * 3),
                'ToDate' => Cbr::datetime(time()),
                'ValutaCode' => $vcommonCode
            ]);

        }
    }

}