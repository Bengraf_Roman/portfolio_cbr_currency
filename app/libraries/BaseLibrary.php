<?php
namespace App\Libraries;


use App\Registry;
use App\Load;


/**
 * Базовый контроллер
 * Class BaseLibrary
 * @package App\Libraries
 */
class BaseLibrary
{
    private static $_instance;
    protected $load;

    public function __construct()
    {
        $this->_registry = Registry::getInstance();
        $this->load = new Load;
    }

    public static function getInstance()
    {

        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}
