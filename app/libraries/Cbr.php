<?php
namespace App\Libraries;



class Cbr extends BaseLibrary
{
    /* Хост веб-сервиса */
    private static $host = 'http://www.cbr.ru/';

    /* Path текущего сервиса */
    private static $service_npath = 'DailyInfoWebServ/DailyInfo.asmx?WSDL';

    /* Instance сервиса */
    private static $service;
    private static $_instance;

    public static $latestDateTime;

    public function __construct()
    {
        parent::__construct();
        self::$service = new \SoapClient(self::$host . self::$service_npath, [
            'soap_version' => SOAP_1_2,
            'exceptions' => false
        ]);
    }

    public static function getInstance()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    /**
     * Вызов метода текущего коннекта с веб-вервером
     * @param $method_name
     * @param array $params
     * @return array|\SimpleXMLElement
     */
    public static function get($method_name, $params = [])
    {
        self::getInstance();
        $service_request = self::$service->{$method_name}($params);

        if ($service_request_resultset = $service_request->{$method_name . 'Result'}) {
            if (isset($service_request_resultset->any)) {
                $resultset = $service_request_resultset->any;
            } else {
                $resultset = $service_request_resultset;
            }
            if ($parsed_result = @simplexml_load_string($resultset)) {
                return $parsed_result;
            } else {
                return $resultset;
            }
        } else {
            return [];
        }
    }

    /**
     * Последнее обновление курсов в timestamp
     * @return int
     */
    public static function getLatestDateTime()
    {
        return strtotime(static::get('GetLatestDateTime', []));
    }

    /**
     * Получение списка курсов по всем валютам на день $timestamp
     * @param $timestamp
     * @return array
     */
    public static function getCursOnDate($timestamp)
    {
        $currencies = static::get('GetCursOnDate', [
            'On_date' => self::datetime($timestamp)
        ])->ValuteData;

        $result = [];
        foreach ($currencies->ValuteCursOnDate as $currency) {
            $result[(string)$currency->VchCode] = $currency;
        }
        return $result;
    }

    /**
     * олучение списка курсов по всем валютам на момент последнего обновление курсов
     * @return array
     */
    public static function getLatestCurs()
    {
        return self::getCursOnDate(self::getLatestDateTime());
    }

    /**
     * Конверт. timestamp в datetime для cbr
     * @param $timestamp
     * @return string
     */
    public static function datetime($timestamp)
    {
        return date('Y-m-d', $timestamp) . 'T' . '00:00:00';
    }


    public static function getCurrencies()
    {
        return $currencies = Cbr::get('EnumValutesXML', [
            'Seld' => false
        ]);
    }

    public static function getFewCurrencies()
    {
        return $currencies = Cbr::get('EnumValutesXML', [
            'Seld' => true
        ]);
    }
}