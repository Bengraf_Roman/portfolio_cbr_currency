<?php
namespace App;

use Exception;

class Load
{

    public function view($name, array $vars = [], $template = 'template')
    {
        $file = BASE_PATH . 'app/views/' . $name . '.php';

        if (is_readable($file)) {
            if (isset($vars)) {
                extract($vars);
            }

            $content = $file;
            require_once(BASE_PATH . "app/views/" . $template . ".php");
            return $content;
        }
        throw new Exception('Не найден файл представления ' . $name . '.php ');
    }

    public function tplGet($name, array $vars = [])
    {
        $file = BASE_PATH . 'app/views/tpl/' . $name . '.php';

        if (is_readable($file)) {
            if (isset($vars)) {
                extract($vars);
            }
            ob_start();
            require_once($file);
            $tpl = ob_get_contents();
            ob_end_clean();
            return $tpl;
        }
        throw new Exception('Не найден файл представления ' . $name . '.php ');

    }
}
