<?php
namespace App\Mvc\Model;

use App\Mvc\Controller\ErrorController;
use PDO;

/**
 * Базовая моедль (PDO)
 * Class BaseModel
 * @package App\Mvc\Model
 */
class BaseModel
{
    /* @var int ID */
    public $id;
    /* @var string Название таблицы */
    protected static $table;
    /* @var PDO БД */
    private static $_db = NULL;

    public function __construct()
    {
        // TODO: Это нужно вынести в loader / services
        $dsn = \Config::getMysqlDsn();
        $user = \Config::getMysqlUser();
        $password = \Config::getMysqlPass();

        self::$_db = new PDO ($dsn, $user, $password, [
            PDO::ATTR_STRINGIFY_FETCHES => false,
            PDO::ATTR_EMULATE_PREPARES => false
        ]);
    }

    /**
     * Инстанс БД
     * @return null|PDO
     */
    public static function DB()
    {
        NULL === self::$_db && new self();
        return self::$_db;
    }

    /**
     * Поиск записи сущности по парметрам
     * @param null $condititons
     * @param null $limit
     * @param array $sort
     * @return array|bool
     */
    public static function find($condititons = null, $limit = null, $sort = null)
    {
        $query = 'SELECT * FROM ' . static::$table;

        if ($condititons && is_array($condititons)) {

            $query .= ' WHERE ';
            $query_set = [];
            foreach ($condititons as $bind_key => $value) {
                $query_set[] = $bind_key . '=:' . $bind_key;
            }
            $query .= implode(" AND ", $query_set);

        } else {
            ErrorController::setError('Невалидный формат запроса...');
        }

        if ($limit && is_numeric($limit)) {
            $query .= ' LIMIT ' . $limit;
        }

        if ($sort && count($sort) == 2) {
            $query .= ' ORDER BY ' . $sort[0] . ' ' . $sort[1];
        }

        $prepare = self::DB()->prepare($query);

        foreach ($condititons as $bind_key => &$value) {
            $prepare->bindValue(':' . $bind_key, $value);
        }

        if ($query_result = $prepare->execute()) {
            $result = $prepare->fetchAll(PDO::FETCH_CLASS, static::class);
        } else {
            return ErrorController::setError('Ошибка запроса...');
        }

        if ($result && count($result) == 1) {
            return $result[0];
        }
        return $result;
    }

    /**
     * Выбор одной записи сущности по параметрам
     * @param null $condititons
     * @return array|bool
     */
    public static function findOne($condititons = null)
    {
        return static::find($condititons, 1);
    }

    /**
     * @param $id
     * @return array|bool
     */
    public static function findById($id)
    {
        if (!is_numeric($id)) {
            return false;
        }
        return static::findOne(['id' => $id]);
    }

    /**
     * Объект класса сущности в массив
     * @return mixed
     */
    public function toArray()
    {
        foreach ($this as $k => $f) {
            $arr[$k] = $f;
        }
        return $arr ?? [];
    }

    /**
     * Сохранение объекта сущности
     * @return bool
     */
    public function save()
    {
        if (isset($this->id) && self::findOne(['id' => $this->id])) {

            $query = 'UPDATE ' . static::$table . ' SET ';
            $query_set = [];

            foreach ($this as $k => $f) {
                $query_set[] .= $k . '=' . '"' . $f . '"';
            }
            $query .= implode(', ', $query_set);
            $query .= ' WHERE id = ' . $this->id;
            $prepare = self::DB()->prepare($query);

            $prepare->bindParam(':id', $this->id, PDO::PARAM_INT);
            foreach ($this as $k => $f) {
                $prepare->bindParam(':' . $k, $f);
            }
        } else {
            $params = array_keys($this->toArray());
            $query = 'INSERT INTO tchk.' . static::$table;
            $query .= '(' . implode(', ', $params) . ')';
            $query .= ' VALUES (:' . implode(', :', $params) . ')';

            $prepare = self::DB()->prepare($query);

            foreach ($this as $k => $f) {
                $prepare->bindValue(':' . $k, $f);
            }
        }

        return $prepare->execute();

    }

    /**
     * Удаление
     * @return bool|\PDOStatement
     */
    public function delete()
    {
        if ($object = self::findById($this->id)) {
            $query = "DELETE FROM " . static::$table . " WHERE id = " . $this->id;
            return self::DB()->query($query);
        } else {
            return false;
        }

    }
}