<?php
namespace App;

use Exception;

/**
 * Маршрутизатор
 * Class Router
 * @package App\Router
 */
class Router
{
    public static function route(Request $request)
    {
        $controller = $request->getController() . 'Controller';
        $method = $request->getMethod();
        $args = $request->getArgs();

        $controllerFile = BASE_PATH . 'app/controllers/' . $controller . '.php';

        if (is_readable($controllerFile)) {
            /** @noinspection PhpIncludeInspection */
            require_once $controllerFile;

            $controller_class = 'App\Mvc\Controller\\' . ucfirst($controller);
            $controller = new $controller_class;

            $method = (is_callable(array($controller, $method))) ? $method : 'indexAction';

            if ($method) {
                if (!empty($args)) {
                    call_user_func_array(array($controller, $method), $args);
                } else {
                    call_user_func(array($controller, $method));
                }
            }
            return;
        }

        throw new Exception('404 - Контроллер ' . $request->getController() . ' не найден', 404);
    }
}
