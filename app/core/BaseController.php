<?php
namespace App\Mvc\Controller;

use App\Mvc\Model\User;
use App\Registry;
use App\Load;
use App\Request;
use App\Session;

/**
 * Базовый контроллер
 * Class baseController
 * @package App\Mvc\Controller
 */
abstract class BaseController
{
    protected $_registry;
    protected $request;
    protected $load;
    protected static $user;

    public function __construct()
    {
        Session::init();
        $this->_registry = Registry::getInstance();
        $this->load = new Load;
        $this->request = new Request;

        static::$user = User::findUser([
            'id' => Session::get('uid')
        ]);

        if (Session::get('token') != md5(Session::get('key')) && $this->request->getController() != 'User') {
            header('Location: ../user');
            exit;
        }
        if (Session::get('token') == md5(Session::get('key')) && $this->request->getController() == 'User' && !$this->request->checkAjax()) {
            header('Location: ../');
            exit;
        }
    }

    public function setResult($data = [])
    {
        echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_HEX_QUOT);
    }

    final public function __get($key)
    {
        if ($result = $this->_registry->$key) {
            return $result;
        }
        return false;
    }
}
