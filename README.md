#### Cbr.ru currencies (SOAP.Api based)
##### December, 2015

### Description

This is a very simple application to get all cost of currencies from cbr.ru, using SOAP-API


### Back-End

This homemade framework on PHP 7. 
Designed according to the MVC paradigm. 
It, of course, there are downsides: no autoloader, a template, the composer, but it's fast - 50ms on a home PC (front-end and back-end).

### Front-end
Nothing special. AJAX(jQuery), try to use OOP-paradigm in JS.

#### To test:

Login: root
Password: root :)

#### My personal rating:

7 / 10

#### Nginx settings:


```
#!nginx

location / {
    rewrite ^/(.*)/?$ /index.php?uri=$1 last;
}
location ~* \.(?:css|js|jpe?g|gif|png)$ { }
location ~ \.php$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/run/php/php7.0-fpm.sock;
}
```