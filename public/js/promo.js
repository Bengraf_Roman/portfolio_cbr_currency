
var core = {};

core.showList = function () {
    var obj = $('[data-target="list"]');
    if (obj.hasClass('hidden')) {
        obj.removeClass('hidden');
    } else {
        obj.addClass('hidden');
    }
};

core.slider = function (direction) {

    var current = $('[data-slide="current"]');
    var next;
    if (direction == 1) {
        next = current.next();
    } else {
        next = current.prev();
    }

    if (next.data('slide') == null) {
        if (direction == 1) {
            next = $('[data-slide]').first();
        } else {
            next = $('[data-slide]').last();
        }
    }
    current.attr('data-slide', '');
    next.attr('data-slide', 'current');
    current.hide();
    next.show();
};
