var admin = {};

admin.transport = function (controller, method, data, callback) {
    if (!controller || !method) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "/" + controller + "/ajax/" + method,
        data: data,
        success: function (msg) {
            console.log(msg);
            if (callback) {
                callback(JSON.parse(msg));
            }
        }
    });
};

admin.handler = function (data) {
    console.log(data);
};

admin.auth = function (data) {
    if (data.result) {
        location.reload();
    } else {
        console.log('Auth failed');
    }
};

admin.logout = function (response) {
    if (response.result) {
        location.reload();
    } else {
        console.log('Logout failed');
    }
};

admin.loadCurr = function () {

    var formData = $('#cur_form').serializeArray();
    formData_count = formData.length;
    admin.pb(0.1);
    for (var i in formData) {
        if (formData[i].value) {
            for (var y = 1; y <= 3; y++) {
                admin.transport('index', 'loadCurrency', {VcommonCode: formData[i].value, y: y}, function () {
                    admin.pb(i * y / (formData_count * 3));
                });
            }
        }
    }

};

admin.pb = function (k) {
    $('[data-pb]').width(100 * k + "%")
}

admin.checkCurr = function (id) {
    var btn = $('button[data-id=' + id + ']');
    var input = $('input[data-id=' + id + ']').first();
    btn.toggleClass('btn-danger active', '');

    if (btn.hasClass('active')) {
        input.val(id);
    } else {
        input.val('');
    }
}

admin.renderResult = function (response) {
    var target_selector = response.target;
    if (target_selector) {
        var target_obj = $('[data-target="' + target_selector + '"]');
        target_obj.html(response.result);
    }
};

admin.loadOtherCur = function (response) {
    $('[data-target="cur"]').show();
    admin.renderResult(response);
    $('[data-btn="load-other"]').hide();
    $('[data-btn="hide-other"]').show();
};

admin.hideOtherCur = function () {
    $('[data-target="cur"]').hide();
    $('[data-btn="load-other"]').show();
    $('[data-btn="hide-other"]').hide();
};

admin.serializeAssoc = function (arr) {
    var data = {};
    $.each(arr.serializeArray(), function (key, obj) {
        var a = obj.name.match(/(.*?)\[(.*?)\]/);
        if (a !== null) {
            var subName = a[1];
            if (!data[subName]) data[subName] = [];
            if (!data[subName][data[subName].length + 1]) data[subName][data[subName].length + 1] = JSON.parse(obj.value);
        } else {
            if (data[obj.name]) {
                if ($.isArray(data[obj.name])) {
                    data[obj.name].push(obj.value);
                } else {
                    data[obj.name] = [];
                    data[obj.name].push(obj.value);
                }
            } else {
                data[obj.name] = obj.value;
            }
        }
    });
    return data;
};