<?php
namespace App;

use App\Mvc\Controller\ErrorController;
ini_set("display_errors", 1);
error_reporting(E_ALL);

/* Корень */
define('BASE_PATH', realpath(dirname(__FILE__)) . '/');/* Подключение необходимых ядерных классов */;

include_once(BASE_PATH . 'app/core/Request.php');
include_once(BASE_PATH . 'app/core/Session.php');
include_once(BASE_PATH . 'app/core/Router.php');
include_once(BASE_PATH . 'app/core/BaseModel.php');
include_once(BASE_PATH . 'app/config/config.php');

foreach (glob("app/models/*.php") as $model_file) {
    include_once $model_file;
}

foreach (glob("app/libraries/*.php") as $lib_file) {
    include_once $lib_file;
}

include_once(BASE_PATH . 'app/libraries/BaseLibrary.php');
include_once(BASE_PATH . 'app/core/BaseController.php');
include_once(BASE_PATH . 'app/core/Load.php');
include_once(BASE_PATH . 'app/core/Registry.php');
include_once(BASE_PATH . 'app/controllers/ErrorController.php');

try {
    Router::route(new Request);
} catch (\Exception $e) {
    $controller = new ErrorController();
    $controller->error($e->getMessage());
}
